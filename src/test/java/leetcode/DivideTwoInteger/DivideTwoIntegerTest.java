package leetcode.DivideTwoInteger;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.core.Is.is;

class DivideTwoIntegerTest {

	private DivideTwoInteger classUnderTest;

	@BeforeEach
	void setUp() {
		classUnderTest = new DivideTwoInteger();
	}

	@Test
	void test1() {
		int result = classUnderTest.divide(10, 3);

		Assert.assertThat(result, is(3));
	}

	@Test
	void test2() {
		int result = classUnderTest.divide(7, -3);

		Assert.assertThat(result, is(-2));
	}


	@Test
	void test3() {
		int result = classUnderTest.divide(-2147483648, -1);

		Assert.assertThat(result, is(2147483647));
	}
}
