package leetcode.addTwoNumbers;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;

class AddTwoNumbersTest {

	@Test
	void case1() {

		ListNode l1 = new ListNode(2);
		ListNode element = new ListNode(4);
		ListNode element2 = new ListNode(3);

		l1.next = element;
		element.next = element2;


		ListNode l2 = new ListNode(5);
		element = new ListNode(6);
		element2 = new ListNode(4);

		l2.next = element;
		element.next = element2;

		AddTwoNumbers classUnderTest = new AddTwoNumbers();
		ListNode result = classUnderTest.addTwoNumbers(l1, l2);

		Assert.assertThat(result.val, is(7));
		Assert.assertThat(result.next.val, is(0));
		Assert.assertThat(result.next.next.val, is(8));
	}

	@Test
	void case2() {

		ListNode l1 = new ListNode(5);

		ListNode l2 = new ListNode(5);

		AddTwoNumbers classUnderTest = new AddTwoNumbers();
		ListNode result = classUnderTest.addTwoNumbers(l1, l2);

		Assert.assertThat(result.val, is(0));
		Assert.assertThat(result.next.val, is(1));
	}

	@Test
	void case3() {

		ListNode l1 = new ListNode(1);
		l1.next = new ListNode(8);

		ListNode l2 = new ListNode(0);

		AddTwoNumbers classUnderTest = new AddTwoNumbers();
		ListNode result = classUnderTest.addTwoNumbers(l1, l2);

		Assert.assertThat(result.val, is(1));
		Assert.assertThat(result.next.val, is(8));
	}
}
