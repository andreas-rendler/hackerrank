package leetcode.medianOfTwoSortedArrays;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;

class MedianOfTwoArraysTest {

	private MedianOfTwoArrays classUnderTest;

	@BeforeEach
	void setUp() {
		classUnderTest = new MedianOfTwoArrays();
	}

	@Test
	void test1() {

		int[] l1 = new int[]{1, 2};
		int[] l2 = new int[]{3, 4};

		double result = classUnderTest.findMedianSortedArrays(l1, l2);

		Assert.assertThat(result, is(2.5d));
	}

	@Test
	void test2() {

		int[] l1 = new int[]{1, 3};
		int[] l2 = new int[]{2};

		double result = classUnderTest.findMedianSortedArrays(l1, l2);

		Assert.assertThat(result, is(2.0d));
	}

	@Test
	void test3() {

		int[] l1 = new int[]{3};
		int[] l2 = new int[]{-2, -1};

		double result = classUnderTest.findMedianSortedArrays(l1, l2);

		Assert.assertThat(result, is(-1d));
	}
}
