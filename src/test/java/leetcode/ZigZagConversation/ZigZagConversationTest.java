package leetcode.ZigZagConversation;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;

class ZigZagConversationTest {

	private ZigZagConversation classUnderTest;

	@BeforeEach
	void setUp() {
		classUnderTest = new ZigZagConversation();
	}

	@Test
	void test1() {
		String input = "ABC";

		String result = classUnderTest.convert(input, 2);

		Assert.assertThat(result, is("ACB"));
	}

	@Test
	void test2() {
		String input = "PAYPALISHIRING";

		String result = classUnderTest.convert(input, 3);

		Assert.assertThat(result, is("PAHNAPLSIIGYIR"));
	}

	@Test
	void test3() {
		String input = "PAYPALISHIRING";

		String result = classUnderTest.convert(input, 4);

		Assert.assertThat(result, is("PINALSIGYAHRPI"));
	}

	@Test
	void test4() {
		String input = "AB";

		String result = classUnderTest.convert(input, 1);

		Assert.assertThat(result, is("AB"));
	}
}
