package hackerrank;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;

class ClimbingLeaderBoardTest {

	@Test
	void testEasy() {
		int[] scores = new int[]{50};

		int[] alice = new int[]{49, 50, 51};

		int[] result = ClimbingLeaderBoard.climbingLeaderboard(scores, alice);

		Assert.assertThat(result.length, is(3));
	}
}
