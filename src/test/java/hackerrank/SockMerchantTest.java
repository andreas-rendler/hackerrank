package hackerrank;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class SockMerchantTest {

	@Test
	void sockMerchant() {

		int pairsLeft = SockMerchant.sockMerchant(5, new int[] {1, 1, 2, 2, 2});

		assertThat(pairsLeft, is(2));
	}
}
