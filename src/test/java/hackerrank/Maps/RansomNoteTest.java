package hackerrank.Maps;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class RansomNoteTest {

	private RansomNote classUnderTest;

	@BeforeEach
	void setUp() {
		classUnderTest = new RansomNote();
	}

	@Test
	void checkMagazineEasy() {

		String[] magazine = new String[]{};
		String[] note = new String[]{};

		boolean result = classUnderTest.checkMagazine(magazine, note);

		assertThat(result, is(true));
	}

	@Test
	void checkMagazineCaseMatch() {

		String[] magazine = new String[]{"test"};
		String[] note = new String[]{"Test"};

		boolean result = classUnderTest.checkMagazine(magazine, note);

		assertThat(result, is(false));
	}

	@Test
	void checkMagazineMatch() {

		String[] magazine = new String[]{"test"};
		String[] note = new String[]{"test"};

		boolean result = classUnderTest.checkMagazine(magazine, note);

		assertThat(result, is(true));
	}

	@Test
	void checkMagazineMatchDuplicates() {

		String[] magazine = new String[]{"test"};
		String[] note = new String[]{"test", "test"};

		boolean result = classUnderTest.checkMagazine(magazine, note);

		assertThat(result, is(false));
	}

	@Test
	void checkMagazineRealTest() {

		String[] magazine = new String[]{"give", "me", "one", "grand", "today", "night"};
		String[] note = new String[]{"give", "one", "grand", "today"};

		boolean result = classUnderTest.checkMagazine(magazine, note);

		assertThat(result, is(true));
	}
}
