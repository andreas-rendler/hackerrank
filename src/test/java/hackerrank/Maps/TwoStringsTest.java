package hackerrank.Maps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class TwoStringsTest {

	private TwoStrings classUnderTest;

	@BeforeEach
	void setUp() {
		classUnderTest = new TwoStrings();
	}

	@Test
	void emptyStrings() {

		String result = classUnderTest.twoStrings("","");

		assertThat(result, is("NO"));
	}

	@Test
	void easyMatch() {

		String result = classUnderTest.twoStrings("a","a");

		assertThat(result, is("YES"));
	}

	@Test
	void mismatch() {

		String result = classUnderTest.twoStrings("hi","world");

		assertThat(result, is("NO"));
	}

	@Test
	void firstStringIsLonger() {

		String result = classUnderTest.twoStrings("bbba","a");

		assertThat(result, is("YES"));
	}

	@Test
	void secondStringIsLonger() {

		String result = classUnderTest.twoStrings("a","bbba");

		assertThat(result, is("YES"));
	}
}
