package hackerrank;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class ComperatorTest {

	@Test
	void firstScenario() {
		Player[] player = new Player[5];
		Checker checker = new Checker();

		player[0] = new Player("amy", 100);
		player[1] = new Player("david", 100);
		player[2] = new Player("heraldo", 50);
		player[3] = new Player("aakansha", 75);
		player[4] = new Player("aleksa", 150);

		Arrays.sort(player, checker);

		assertThat(player[0].name, is("aleksa"));
		assertThat(player[1].name, is("amy"));
		assertThat(player[2].name, is("david"));
		assertThat(player[3].name, is("aakansha"));
		assertThat(player[4].name, is("heraldo"));
	}

	@Test
	void secondScenario() {
		Checker checker = new Checker();
		Player[] player = new Player[3];
		player[0] = new Player("smith", 20);
		player[1] = new Player("jones", 15);
		player[2] = new Player("jones", 20);

		Arrays.sort(player, checker);

		assertThat(player[0].name, is("jones"));
		assertThat(player[0].score, is(20));
		assertThat(player[1].name, is("smith"));
		assertThat(player[1].score, is(20));
		assertThat(player[2].name, is("jones"));
		assertThat(player[2].score, is(15));
	}
}
