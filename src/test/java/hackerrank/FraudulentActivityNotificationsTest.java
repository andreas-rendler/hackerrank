package hackerrank;

import hackerrank.FraudulentActivityNotifications;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class FraudulentActivityNotificationsTest {

	@Test
	void activityNotifications() {

		int day = 4;
		int[] expenditure = new int[]{1, 2, 3, 4, 4};

		int result = FraudulentActivityNotifications.activityNotifications(expenditure, day);

		assertThat(result, is(0));
	}

	@Test
	void activityNotificationsTwo() {

		int day = 5;
		int[] expenditure = new int[]{2, 3, 4, 2, 3, 6, 8, 4, 5};

		int result = FraudulentActivityNotifications.activityNotifications(expenditure, day);

		assertThat(result, is(2));
	}

	@Test ()
	void activationNotificationsTest1() {

		//n=200000 d=10000

		int day = 10000;

		int[] expenditure = new int[0];

		try {
			String contents = new String(Files.readAllBytes(Paths.get(
					"src/test/java/FraudulentActivityTest_1.txt")));
			contents = contents.replace("\n", "");
			expenditure = Stream.of(contents.split(" ")).mapToInt(Integer::parseInt).toArray();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		int result = FraudulentActivityNotifications.activityNotifications(expenditure, day);

		assertThat(result, is(633));
	}

	@Test ()
	void activationNotificationsTest5() {
		int day = 40001;

		int[] expenditure = new int[0];

		try {
			String contents = new String(Files.readAllBytes(Paths.get(
					"src/test/java/testDataForFraudulentNotification.txt")));
			contents = contents.replace("\n", "");
			expenditure = Stream.of(contents.split(" ")).mapToInt(Integer::parseInt).toArray();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		int result = FraudulentActivityNotifications.activityNotifications(expenditure, day);

		assertThat(result, is(926));
	}
}
