package leetcode.addTwoNumbers;

// https://leetcode.com/problems/add-two-numbers/

public class AddTwoNumbers {

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode result = null;
		ListNode currentNode = null;
		boolean overflow = false;

		do {
			int sum1 = 0;
			int sum2 = 0;

			if(l1 != null) {
				sum1 = l1.val;
			}

			if(l2 != null) {
				sum2 = l2.val;
			}

			int sum = sum1 + sum2;

			if(overflow) {
				sum += 1;
				overflow = false;
			}

			if(sum > 9) {
				sum = sum % 10;
				overflow = true;
			}

			ListNode newNode = new ListNode(sum);


			if (result == null) {
				result = newNode;
				currentNode = newNode;
			}
			else {
				currentNode.next = newNode;
				currentNode = newNode;
			}

			if(l1 != null) {
				l1 = l1.next;
			}
			if(l2 != null) {
				l2 = l2.next;
			}

		} while (l1 != null || l2 != null);

		if(overflow) {
			currentNode.next = new ListNode(1);
		}

		return result;
	}
}

