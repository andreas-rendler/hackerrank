package leetcode.DivideTwoInteger;

// https://leetcode.com/problems/divide-two-integers/

public class DivideTwoInteger {

	public int divide(int dividend, int divisor) {

		int result = dividend / divisor;

		if(dividend == Integer.MIN_VALUE && divisor == -1) {
			result = Integer.MAX_VALUE;
		}

		return result;
	}
}
