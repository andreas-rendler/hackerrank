package leetcode.medianOfTwoSortedArrays;

// https://leetcode.com/problems/median-of-two-sorted-arrays/

public class MedianOfTwoArrays {
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {

		int combinedLength = nums1.length + nums2.length;

		int[] combinedArray = new int[combinedLength];
		int pos1 = 0;
		int pos2 = 0;
		int posCombined = 0;

		while (pos1 < nums1.length || pos2 < nums2.length) {

			if(pos1 >= nums1.length) {
				combinedArray[posCombined] = nums2[pos2];
				pos2++;
			} else if(pos2 >= nums2.length) {
				combinedArray[posCombined] = nums1[pos1];
				pos1++;
			} else if (pos1 < nums1.length && nums1[pos1] <= nums2[pos2]) {
				combinedArray[posCombined] = nums1[pos1];
				pos1++;
			} else if (pos2 < nums2.length && nums1[pos1] > nums2[pos2]) {
				combinedArray[posCombined] = nums2[pos2];
				pos2++;
			}
			posCombined++;
		}

		double median = 0.0d;

		if (combinedLength % 2 == 0) {
			median = (combinedArray[combinedLength / 2-1] + combinedArray[combinedLength / 2]) / 2d;
		}
		else {
			median = combinedArray[(combinedLength / 2)];
		}

		return median;
	}
}
