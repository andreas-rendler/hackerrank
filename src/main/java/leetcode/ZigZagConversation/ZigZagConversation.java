package leetcode.ZigZagConversation;

// https://leetcode.com/problems/zigzag-conversion/

public class ZigZagConversation {
	public String convert(String s, int numRows) {

		if(numRows == 1) {
			return s;
		}

		int length = (s.length() / 2) + 1;

		char[][] matrix = new char[numRows][length];

		boolean directionDown = true;
		int x = 0;
		int y = 0;

		for (int i = 0; i < s.length(); i++) {

			matrix[y][x] = s.charAt(i);

			if (directionDown) {

				if (y >= numRows - 1) {
					directionDown = false;
					y--;
					x++;
				}
				else {
					y++;
				}
			}
			else {

				if (y < 1) {
					directionDown = true;
					y++;
				} else {
					y--;
					x++;
				}
			}
		}

		StringBuilder result = new StringBuilder();

		for (int j = 0; j < numRows; j++) {
			for (int i = 0; i < length; i++) {

				char c = matrix[j][i];

				if (c != '\u0000') {
					result.append(c);
				}
			}
		}

		return result.toString();
	}
}
