package hackerrank;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SockMerchant {
	public static int sockMerchant(int n, int[] ar) {

		Map<Integer, Integer> integerMap = getIntegerMap(ar);

		Integer leftSocks = integerMap.values().stream().map(x -> x % 2).reduce(0, Integer::sum);

		return (ar.length - leftSocks) / 2;
	}

	private static Map<Integer, Integer> getIntegerMap(int[] ar) {
		Map<Integer, Integer> integerMap = new HashMap<>();
		Arrays.stream(ar).forEach(i -> {
			if (integerMap.containsKey(i)) {
				integerMap.put(i, integerMap.get(i) + 1);
			}
			else {
				integerMap.put(i, 1);
			}
		});
		return integerMap;
	}
}
