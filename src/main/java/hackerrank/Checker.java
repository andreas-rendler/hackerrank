package hackerrank;

import java.util.Comparator;

public class Checker implements Comparator<Player> {
	// complete this method
	public int compare(Player a, Player b) {

		int result = b.score - a.score;

		if (result == 0) {
			result = a.name.compareTo(b.name);
		}
		return result;
	}
}
