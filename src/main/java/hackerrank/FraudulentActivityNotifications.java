package hackerrank;

public class FraudulentActivityNotifications {

	public static int activityNotifications(int[] expenditure, int days) {

		int lengthOfExpenditure = expenditure.length;
		int fraudulentNotifications = 0;
		int[] countArray = new int[201];
		boolean first_time = true;
		int pop_element = 0;

		for (int i = 0; i <= days - 2; i++) {
			countArray[expenditure[i]]++;
		}
		countArray[pop_element]++;

		for (int index = days; index < lengthOfExpenditure; index++) {

			countArray[pop_element]--;
			countArray[expenditure[index - 1]]++;

			int median = getMedian(countArray, days);

			if (days % 2 == 0) {
				if (expenditure[index] >= median)
					fraudulentNotifications++;
			} else {
				if (expenditure[index] >= 2 * median)
					fraudulentNotifications++;
			}

			pop_element = expenditure[index - days];
		}

		return fraudulentNotifications;
	}

	private static int getMedian(int[] countArray, int days) {
		int[] prefix_sum = new int[201];
		prefix_sum[0] = countArray[0];

		for (int i = 1; i < 201; i++) {
			prefix_sum[i] = prefix_sum[i - 1] + countArray[i];
		}

		int median = 0;


		if (days % 2 == 0) {
			int middleLeftIndex = 0;
			int middleRightIndex = 0;
			int first = days / 2;
			int second = first + 1;
			int i = 0;

			for (; i < 201; i++) {
				if (first <= prefix_sum[i]) {
					middleLeftIndex = i;
					break;
				}
			}
			for (; i < 201; i++) {
				if (second <= prefix_sum[i]) {
					middleRightIndex = i;
					break;
				}
			}
			median = middleLeftIndex + middleRightIndex;
		}
		else {
			int first = days / 2 + 1;
			for (int i = 0; i < 201; i++) {
				if (first <= prefix_sum[i]) {
					median = i;
					break;
				}
			}
		}

		return median;
	}
}
