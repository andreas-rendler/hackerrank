package hackerrank.Maps;

import java.util.Set;
import java.util.stream.Collectors;

public class TwoStrings {

	String twoStrings(String s1, String s2) {

		Set<Character> set1 = s1.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
		Set<Character> set2 = s2.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());

		long isMatch = set1.stream().filter(x -> !s2.isEmpty() && s2.contains(String.valueOf(x))).count();

		return isMatch > 0 ? "YES" : "NO";
	}
}
