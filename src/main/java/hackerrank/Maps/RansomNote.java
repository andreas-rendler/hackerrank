package hackerrank.Maps;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RansomNote {

	boolean checkMagazine(String[] magazine, String[] note) {

		boolean result = true;

		Map<String, Integer> magazineMap = getStringIntegerMap(magazine);
		Map<String, Integer> noteMap = getStringIntegerMap(note);

		for (Map.Entry<String, Integer> entry: noteMap.entrySet()) {

			if (magazineMap.get(entry.getKey()) == null || entry.getValue() > magazineMap.get(entry.getKey())) {
				result = false;
			}
		}


		return result;
	}

	private Map<String, Integer> getStringIntegerMap(String[] magazine) {
		Map<String, Integer> magazineMap = new HashMap<>();
		Arrays.stream(magazine).forEach(s -> {
			if (magazineMap.containsKey(s)) {
				magazineMap.put(s, magazineMap.get(s) + 1);
			}
			else {
				magazineMap.put(s, 1);
			}
		});
		return magazineMap;
	}
}
